# CronJob Monitor

## Getting started

CronJob Monitoring Operator implements continuously monitoring CronJobs. It will be looking for cronjob with missing scheduling periods or, have been successfully scheduled but failed to execute (non-zero exit code).

Practical case is sound as:

"We run a couple of mission-critical batch workloads that are being scheduled every night. These workloads are started by Kubernetes CronJobs. It is important to make sure that these workloads are scheduled and executed until completion. We currently don’t have any visibility into this. We don’t know if something has happened in Kubernets and the job wasn’t scheduled. We also don’t know if the job has finished successfully or not."

So this operator should expose the following Prometheus metric (over HTTP):

```
cron_job_failures{namespace=“env-production”, job_name=“important-batch-process”} 0
cron_job_failures{namespace=“env-production”, job_name=“another-batch-process”} 1
```

Where the value of the metric indicates if there is a problem with a job. “0” means the job has ran to successful completion. “1” means the last time the job was supposed to run it either did not run or excited with non-zero code.

In order to focus on checking the practical case, it was decided to use a framework to create an operator - KubeBuilder. 

## Requirements

Kubernetes version: 1.21.5 (should work with upper versions but not tested yet).

## Installation

Regardless of the installation option, you need to configure access to Kybernetes with cluster-admin rights.

This is necessary for Custom Resource Definition installation.

### Run operator locally

The operator can be run on a workstation without installation in Kubernetes.
To do this, download the repository, go to the directory.
Specify the version of the operator through the definition of the environment variable IMG.
And run the installation through the ```make``` utility.

```bash
$ git clone https://gitlab.com/SablinIgor/cronjob-monitor.git

$ cd cronjob-monitor

$ export IMG=soaron/controller:0.0.5

$ make install
$ make run
```

### Deploy in Kubernetes

Installation to the Kubernetes cluster is performed in a similar way and differs only in the ```make deploy``` command, not ```make run```.

```bash
$ git clone https://gitlab.com/SablinIgor/cronjob-monitor.git

$ cd REPO

$ export IMG=soaron/controller:0.0.5

$ make install
$ make deploy
```

### Install cronjob examples

To test the operator, you can use two cronjob manifests in the examples directory.
Cronjob in the crj.yaml works without problems.
Cronjob in the crj-fail.yaml fails with an error.

```bash
$ kubectl apply -f examples                                                    
cronjob.batch/hello-failed created
cronjob.batch/hello created
serviceaccount/debugger created
clusterrolebinding.rbac.authorization.k8s.io/manager-rolebinding created
```

## Usage

In the case of a local launch of the operator, you can check the availability of metrics with the following request:

If you run controller on local get Prometheus metrics by:
```bash
$ curl localhost:8080/metrics | grep cron_job_failures
```

If the operator is installed in the Kubernetes cluster, then you can check for metrics as follows (you will also need to apply the examples/rbac.yaml manifest):

```bash
$ kubectl run bash --image=bash --overrides='{ "spec": { "serviceAccount": "debugger" }  }' -- sleep 3600 

$ kubectl exec -it bash -- bash

$ bash-5.1# apk add curl

$ bash-5.1# export TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)

$ bash-5.1# curl -ks -H "Authorization: Bearer ${TOKEN}" https://cronjob-monitor-controller-manager-metrics-service.cronjob-monitor-system:8443/metrics | grep cron_job_failures
# HELP cron_job_failures Flag success or failed job
# TYPE cron_job_failures gauge
cron_job_failures{job_name="hello",job_namespace="default"} 0
cron_job_failures{job_name="hello-failed",job_namespace="default"} 1
```