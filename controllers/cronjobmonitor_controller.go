/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"

	cjmv1alpha1 "cronjob-monitor/api/v1alpha1"
	"github.com/prometheus/client_golang/prometheus"
	kbatch "k8s.io/api/batch/v1"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/metrics"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

// CronjobMonitorReconciler reconciles a CronjobMonitor object
type CronjobMonitorReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

var (
	cron_job_failures = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "cron_job_failures",
			Help: "Flag success or failed job",
		},
		[]string{"job_namespace", "job_name"},
	)
)

func init() {
	metrics.Registry.MustRegister(cron_job_failures)
}

//+kubebuilder:rbac:groups=cjm.sablin.de,resources=cronjobmonitors,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=cjm.sablin.de,resources=cronjobmonitors/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=cjm.sablin.de,resources=cronjobmonitors/finalizers,verbs=update
//+kubebuilder:rbac:groups=batch,resources=jobs,verbs=list

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the CronjobMonitor object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.10.0/pkg/reconcile
func (r *CronjobMonitorReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	l := log.FromContext(ctx)

	l.Info("Enter Reconcile", "req", req)

	job := &kbatch.Job{}
	_ = r.Client.Get(context.TODO(), req.NamespacedName, job)

	//l.Info("Get job", "job", job.Spec)
	//l.Info("Get job", "job", job.Status)
	owner := metav1.GetControllerOf(job)
	//l.Info("Get job owner kind", "job", owner.Kind)
	//l.Info("Get job owner name", "job", owner.Name)

	if owner != nil {
		if owner.Kind == "CronJob" {
			for _, c := range job.Status.Conditions {
				if c.Type == kbatch.JobComplete && c.Status == v1.ConditionTrue {
					l.Info("Job Status", "job", "Success!!!")
					l.Info("Job Owner", "job", owner.Name)
					cron_job_failures.WithLabelValues(req.Namespace, owner.Name).Set(0)
				} else if c.Type == kbatch.JobFailed && c.Status == v1.ConditionTrue {
					l.Info("Job Status", "job", "Failed!!!")
					l.Info("Job Owner", "job", owner.Name)
					cron_job_failures.WithLabelValues(req.Namespace, owner.Name).Set(1)
				}
			}
		}
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *CronjobMonitorReconciler) SetupWithManager(mgr ctrl.Manager) error {
	// Create a new controller with the reconciler.
	c, err := controller.New("cronjobs-controller", mgr, controller.Options{
		Reconciler: r,
	})

	err = c.Watch(
		&source.Kind{Type: &kbatch.Job{}},
		&handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	/*err = c.Watch(
		&source.Kind{Type: &kbatch.Job{}},
		&handler.EnqueueRequestForOwner{
			IsController: true,
			OwnerType:    &kbatch.CronJob{}})
	if err != nil {
		return err
	}*/

	return ctrl.NewControllerManagedBy(mgr).
		For(&cjmv1alpha1.CronjobMonitor{}).
		Complete(r)
}
